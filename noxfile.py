import nox
import tempfile
from nox_poetry import session

nox.options.sessions = "lint", "safety", "tests"

locations = "src", "tests", "noxfile.py"


@session(python=["3.9"])
def tests(session):
    args = session.posargs or ["--cov", "-m", "not e2e"]
    session.install("pytest", "pytest-cov", "pytest-mock", ".")
    session.run("pytest", *args)


@session(python=["3.9"])
def lint(session):
    args = session.posargs or locations
    session.install(
        "flake8",
        "flake8-bandit",
        "flake8-black",
        "flake8-bugbear",
        "flake8-import-order",
    )
    session.run("flake8", *args)


@session(python="3.9")
def black(session):
    args = session.posargs or locations
    session.install("black")
    session.run("black", *args)


@session(python="3.9")
def safety(session):
    with tempfile.NamedTemporaryFile() as requirements:
        session.run(
            "poetry",
            "export",
            "--dev",
            "--format=requirements.txt",
            "--without-hashes",
            f"--output={requirements.name}",
            external=True,
        )
        session.install("safety")
        session.run("safety", "check", f"--file={requirements.name}", "--full-report")
